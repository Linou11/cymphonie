<div id="footer" class="d-flex flex-column flex-sm-row text-bg-dark justify-content-between py-4 my-4 border-top">
      <p>© 2022 Cymphonie, Inc. All rights reserved.</p>
      <ul class="list-unstyled d-flex">
    <?php
    if($_SESSION == 'admin' && $page =='boutique'){?>
        <li class="ms-3"><a class="btn btn-secondary" href="../VIEW/modifierPdt.php">Modifier les produits<svg class="bi" width="24" height="24"><use xlink:href=""></use></svg></a></li>
        <li class="ms-3"><a class="btn btn-secondary" href=../CONTROL/updatePdtStock.php>updateProduits<svg class="bi" width="24" height="24"><use xlink:href=""></use></svg></a></li>
        <li class="ms-3"><a class="btn btn-secondary" href=../VIEW/addProduit.php>Add Produit<svg class="bi" width="24" height="24"><use xlink:href=""></use></svg></a></li>
        
        <?php }
    else {?>
    <?php } 
    ?> 

        <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"></use></svg></a></li>
        <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"></use></svg></a></li>
        <li class="ms-3"><a class="link-dark" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"></use></svg></a></li>
    </ul>
</div>