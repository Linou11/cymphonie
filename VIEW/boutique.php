<?php 
$_SESSION = 'client';
?>
<!DOCTYPE html>
<html lang="en">
<head> 
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<?php 
$page='boutique';
include '../VIEW/navbar.php';
include '../MODEL/readAll.php'
?>


<div class="album py-5 bg-light card1">
    <div class="container content">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
      <?php


foreach($repPdt as $data){
?>

<div class="col">
  <div class="card shadow-sm">
    <img id="imgwh" width="100%" height="250px" src="<?= $data['image']?>"></svg>
    <div class="card-body" opacity="0.7">
    <p><h2 class="fw-normal"><?= $data['nom'] ?></h2></p>
      <p class="card-text"><?= $data['description'] ?></p>
      <div class="d-flex justify-content-between align-items-center">
        <div class="btn-group">
          <button type="button" class="btn btn-sm btn-outline-secondary">
          <form action="../CONTROL/ajouter.php" method="post">
            <input type="hidden" name="id_prod" value="<?= $data['id'] ?>">
            <input type="hidden" name="id" value="<?= $data['id'] ?>">
            <input name="qt" type="hidden" value="1">
            <input name="dispo" type="hidden" value="0">
            <?php if ($data['dispo'] == 1) {?>
                <button type="submit" class="btn btn-sm btn-outline-secondary">Ajouter au panier</button>
                <?php }
        else {?>Produit indisponible
        <?php }?>
    </p>
    <button class="btn btn-sm btn-outline-secondary" href="../CONTROL/updatePdtStock.php">-</button>


            </form>
          </button>
        </div>
        <small class="text-muted"><?= $data['prix'] ?> €</small>
      </div>
    </div>
  </div>
</div>
    


<?php } ?>
    
</div>
    <a class="btn btn-secondary" href="panier.php">Valider mon panier</a></br>

</div>

<?php include 'footer.php';?>
</div>
</body>
</html>