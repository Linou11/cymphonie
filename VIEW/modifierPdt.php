<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<body>
<div class="content">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    <?php include '../VIEW/navbar.php'; ?>
    <?php include '../MODEL/readAll.php'; ?>
    
    <?php
    $page='modif';


        foreach($repPdt as $data){
    ?>

    <form action="../CONTROL/updatePdt.php" method="post" enctype="multipart/form-data">
<div id="liste">
<label for='id'>Produit : <?= $data['nom']?></label>
    <input type="hidden" value="<?= $data['id']?>" name="id"></br>


<label for="nom">Nom :</label>
<input type="text" name="nom" required><br>

<label for="image">URL de l'image :</label>
<input type="text" name="image"><br>

<label for="description">Informations :</label>
<input type="text" name="description"></textarea><br>

<label for="son">Son :</label>
<input type="text" name="son"><br>

<label for="prix">Prix :</label>
<input type="text" name="prix"><br>

<label for="dispo">Disponibilité :</label>
<input type="checkbox" name="dispo"><br>

<input type="submit" value="Modifier">
</form>
<?php } ?>

</form>
<?php include 'footer.php'; ?>
</div>
</body>
</html>