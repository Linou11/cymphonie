<?php 
require '../MODEL/debug.php';
include '../CONTROL/data.php';
?>
    <?php $_SESSION = "admin"; ?>
<div id="header" class="px-3 py-2 text-bg-dark">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <a href="homePage.php" class="d-flex align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none">
            <svg class="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><h1>CYMPHONIE</h1><use xlink:href="#bootstrap"></use></svg>
          </a>

          <ul class="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
            <li>
              <a href="../VIEW/info.php" class="nav-link text-secondary">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24"><use xlink:href="#home"></use></svg>
                Info
              </a>
            </li>
            <li>
              <a href="../VIEW/boutique.php" class="nav-link text-white">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24"><use xlink:href="#grid"></use></svg>
                Boutique
              </a>
            </li>
            <li>
              <a href="../VIEW/panier.php" class="nav-link text-white">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24"><use xlink:href="#speedometer2"></use></svg>
                Panier
              </a>
            </li>
            <!-- <li>
              <a href="../VIEW/listeDesCommandesClients.php" class="nav-link text-white">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24"><use xlink:href="#table"></use></svg>
                Commandes
              </a>
            </li> -->
           <?php
            if($_SESSION == "admin"){?>
    
            <li>
              <a href="../VIEW/listeDesCommandesAdmin.php" class="nav-link text-white">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24"><use xlink:href="#table"></use></svg>
                Commandes Admin
              </a>
            </li>
            <li>
              <a href="../VIEW/Clients.php" class="nav-link text-white">
                <svg class="bi d-block mx-auto mb-1" width="24" height="24"><use xlink:href="#people-circle"></use></svg>
                Clients
              </a>
            </li>
    <?php }
    else {?>
<?php }
?>  
          </ul>
        </div>
      </div>
    </div>

</div>
<!-- <script src="./change.js"></script> -->
