<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
   
<?php 
$page='panier';
include '../VIEW/navbar.php';
include '../MODEL/readAll.php';
include '../MODEL/model.php';
?>
    <div class="album py-5 bg-light card1">
        <div class="container content">
    
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
    
    <?php
if(!isset($_COOKIE['id_commande'])){ ?>
    <div class="card shadow-sm center" > <?php echo "Pas de commande en Cours"; ?></div>
    <?php
}else{

$req = $pdo->prepare('select * from produit_commande where id_commande = ?;');
$req->execute([$_COOKIE['id_commande']]);
$mesInfos = $req->fetchAll();

$total = 0;
?>
<?php
foreach($mesInfos as $data){
    $prod = $pdo->query("select * from produit where id = ${data['id_produit']};")->fetch();
    $total += $prod['prix'] * $data['quantite'];
    ?>
                
                
        <div class="col">
            <div class="card shadow-sm">
                <div class="card-body">
                    <p><h1><?= $prod['nom'] ?></h1></p>
                    <p><h2 class="fw-normal"><?= $prod['prix'] ?> €</h2></p>

                    
                </div>
                <?php } ?>
                <p class="border">TOTAL : <?= $total ?> €</p>
              
    <?php } ?>
            </div>
        </div>
        <?php if(!isset($_COOKIE['id_commande'])){ 

        } else { ?>
        
            <div> 
                <form action="validation-panier.php" method="post">
                    <input type="text" name="nom" placeholder="nom">
                    <input type="text" name="mail" placeholder="mail">
                    <input type="text" name="tel" placeholder="telephone">
                    <input type="submit" value="VALIDER MA COMMANDE">
                </form>
            </div>
        </div>
    </div>
    <?php } ?>
        <div>
            <a class="btn btn-secondary" href=../VIEW/boutique.php>Retour à la boutique</a>
        </div>          
    
</div>
<?php include 'footer.php'; ?>
            </body>
            </html>
