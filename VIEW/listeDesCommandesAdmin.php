<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    
    <?php 

$page = 'Commandes';
include '../MODEL/model.php';
include '../VIEW/navbar.php';
include '../MODEL/readAll.php';

?>
<div class="album py-5 bg-light card1">
<div class="container content">
    <div id="liste">
        <label>Selection par état : </label>
        <form action="../VIEW/listeDesCommandesAdmin.php" method="get">
        <select id="etat" name="etat">
            <option value="panier">Panier</option>
            <option value="validée">Validée</option>
            <option value="prete">Prête</option>
            <option value="collectée">Collectée</option>
        </select>
        <input type="submit" value="Afficher">
        </form>
    </div>
    <?php 
 if(! isset($_GET['etat'])){
    $etat = 'validée';
 }else{
    $etat=$_GET['etat'];
 }

if ($etat != 'panier') {
$req = $pdo->prepare('SELECT commande.id as id_cmd,  produit.nom as nom_prod, client.nom as nom_client from commande 
INNER JOIN produit_commande on produit_commande.id_commande = commande.id 
INNER JOIN produit ON produit_commande.id_produit = produit.id 
INNER JOIN client on commande.id_client = client.id;
where commande.etat = ?;');
$req->execute([$etat]);

$repCmd = $req->fetchAll();
} else {
$req = $pdo->prepare('SELECT commande.id as id_cmd, produit.nom as nom_prod from commande 
INNER JOIN produit_commande on produit_commande.id_commande = commande.id 
INNER JOIN produit ON produit_commande.id_produit = produit.id 
where commande.etat = ?;');
$req->execute([$etat]);
$repCmd = $req->fetchAll();
}

        foreach($repCmd as $data){
    ?>

<div class="col">
  <div class="card shadow-sm">
    <div class="card-body">
        <p><h1>Commande n° <?= $data['id_cmd'] ?></h1></p>
    <p><h2 class="fw-normal"><?= $data['nom_prod'] ?></h2></p>
    <?php if ($etat != 'panier') {?>
    <p><h3 class="fw-normal"><?= $data['nom_client'] ?></h3></p>
    <?php } ?>
</div>
</div>
<?php } ?>
</div>
</div>
</div>
<?php include '../VIEW/footer.php'; ?>
</body>
</html>