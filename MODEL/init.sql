drop database if exists cymphonie;
create database cymphonie;
use cymphonie;

create table client (
    id int auto_increment primary key,
    mail varchar(255) not null,
    nom varchar(255) not null,
    tel varchar(255) not null
);

create table produit(
    id int auto_increment primary key,
    image text not null,
    nom varchar(255) not null,
    prix float not null,
    son text not null,
    description text not null,
    dispo boolean
);

create table commande(
    id int auto_increment primary key,
    id_client int,
    etat enum('panier', 'validée', 'prete', 'collectée') default 'panier'
);

create table produit_commande(
    id_commande int,
    id_produit int,
    quantite float
);



drop user if exists toto@'localhost';
create user toto@'localhost' identified by 'mdp1234';
grant all privileges on cymphonie.* to toto@'localhost';
