<?php
include 'pdo.php';

function createCymbale($description, $nom, $image, $prix, $son, $dispo){
    global $pdo;
    $req = $pdo->prepare('insert into produit (description, nom, image, prix, son, dispo) values (?,?,?,?,?,?);');
    $req->execute([$description, $nom, $image, $prix, $son, $dispo]);
}

function createClient($nom, $email, $telephone){
    global $pdo;
    $req = $pdo->prepare('insert into client(nom, email, telephone) values (?, ?, ?);');
    $req->execute([$nom, $email, $telephone]);
};

function readClient($id){
    global $pdo;
    $req = $pdo->prepare("select * from client where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};

function updatePdt($nom, $image, $description, $son ,$prix, $dispo, $id){
    global $pdo;
    $req = $pdo->prepare("update produit set nom=?, image=?, description=?, son=? ,prix=?, dispo=? where id=?;");
    $req->execute([$nom, $image, $description, $son, $prix, $dispo, $id]);
};

function readProduit($id){
    global $pdo;
    $req = $pdo->prepare("select * from produit where id=?;");
    $req->execute([$id]);
    return $req->fetchAll();
};


function createPanier($id_produit, $id_commande, $quantite){
    global $pdo;
    $req = $pdo->prepare('insert into panier(id_produit, id_commande, quantite) values (?, ?, ?);');
    $req->execute([$id_produit, $id_commande, $quantite]);
};

function readPanier($id_commande){
    global $pdo;
    $req = $pdo->prepare("select * from panier where id_commande=?;");
    $req->execute([$id_commande]);
    return $req->fetchAll();
}

?>